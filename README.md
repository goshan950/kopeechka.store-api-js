Данный код является представлением API kopeechka.store (https://bit.ly/3mVpuxF) на JavaScript.
Ниже будут описаны ряд функций, которые чаще всего используются в работе.

Перед работой необходимо установить:
`npm install xmlhttprequest`

Инициализируем класс:
`mail = new kopeechka("YOUR_TOKEN");`

Пример запроса баланса с токеном:

```
console.log("BALANCE IS " + mail.getBalance()['balance']);
```

Пример покупки ящика:
```
var response = mail.getEmail({"mail_type":"mail.ru","site":"yandex.ru"});
console.log("STATUS: " + response['status'] + " ACTIVATION ID:" + response['id'] + " MAIL.RU EMAIL FOR YANDEX.RU SITE:" + response['mail']);
```

Пример поиска письма:
```
var response = mail.getMessage({"full":1,"id":ACTIVATION_ID}); // ACTIVATION_ID = response['id'] из запросе getEmail
console.log("STATUS: " + response['status'] + " FULL_MESSAGE:" + response['fullmessage'] + " VALUE (automatically parsed link, may be null):" + response['value']);
```

Пример отмены активации:
```
var response = mail.cancelEmail({"id":ACTIVATION_ID}); // ACTIVATION_ID = response['id'] из запросе getEmail
```

