class kopeechka{
	
    constructor(token = '')
	{
		
		this.token = token;
		this.link = 'http://api.kopeechka.store/';
	}
	
	request(link, method = 'GET', data = null)
	{
		var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
		var request = new XMLHttpRequest();
		request.open(method, link, false);
		
		request.setRequestHeader('Access-Control-Allow-Origin', this.link);
		request.setRequestHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST');
		request.setRequestHeader('Access-Control-Allow-Headers', 'Content-Type, User-Agent, Cache-Control');
		request.send(data);
		
		if(request.status == 200) return JSON.parse(request.responseText);
		else return {'status':'ERROR', 'value' : 'status response error'}
	}
	
	getString(data = {})
	{
		var keys = Object.keys(data); var req_string = '';
		keys.forEach(function get_string(value) {req_string += "&"+value+"="+data[value];})
		return req_string;
	}

	getFullLink(method = null, data = {})
	{
		return this.link+method+"?token="+this.token+this.getString(data)
	}
	
	getBalance() // Получить баланс
	{ 
		return this.request(this.link+"user-balance?token="+this.token);
	}

	getEmail(data = {}) // Заказать ящик
	{
		return this.request(this.getFullLink("mailbox-get-email",data));
	}
	
	getDomains(data = {}) // Получить список доменов
	{
		return this.request(this.getFullLink("mailbox-get-domains",data));
	}
	
	getMessage(data = {}) // Получить письмо
	{
		return this.request(this.getFullLink("mailbox-get-message",data));
	}
	
	cancelEmail(data = {}) // Отменить активацию
	{
		return this.request(this.getFullLink("mailbox-cancel",data));
	}
	
	reorderMessage(data = {}) // Перезаказать письмо
	{
		return this.request(this.getFullLink("mailbox-reorder",data));
	}

	getFreshId(data = {}) // Получить актуальный ID вашего заказа (вдруг делали его давно и надо вспомнить, а под рукой только сайт и почта)
	{
		return this.request(this.getFullLink("mailbox-get-fresh-id",data));
	}

	setComments(data ={}) // Установить комментарий на активацию
	{
		return this.request(this.getFullLink("mailbox-set-comment",data));
	}

	getBulk(data ={}) // Поиск по параметрам
	{
		return this.request(this.getFullLink("mailbox-get-bulk",data));
	}

	addToDomainsBlacklist(data ={}) // Добавить домен в чёрный список для сайта
	{
		return this.request(this.getFullLink("domain-add-blacklist",data));
	}
	
	excludeFromDomainsBlacklist(data ={}) // Исключить домен из чёрного списка для сайта
	{
		return this.request(this.getFullLink("domain-exclude-blacklist",data));
	}

	getDomainsBlacklist(data ={}) // Получить чёрный список для сайта
	{
		return this.request(this.getFullLink("blacklist-get-service",data));
	}

	getMailboxZones(data ={}) // Получить доменные зоны с ценами
	{
		return this.request(this.getFullLink("mailbox-zones",data));
	}

	getMailboxDomains(data ={}) // Получить доступные домены
	{
		return this.request(this.getFullLink("mailbox-get-domains",data));
	}

	getAliveDomains(data ={}) // Получить срок жизни домена
	{
		return this.request(this.getFullLink("domain-get-alive",data));
	}
}
